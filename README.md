# Portfolio prog2006 - Advanced Programming spring 2024

This README documents the portfolio structure for the course prog2006, and contains links to all projects completed this spring semester. You'll also find a link to the demo video. 
The portfolio projects are all stored and organized in the "Portfolio prog2006" group on GitLab.  

#### *Link to entire portfolio group:*

[Portfolio](https://gitlab.com/portfolio-prog2006)

## Portfolio Contents/Projects

### Haskell projects
- Lab01 - basics of Haskell - [Link Lab01](https://gitlab.com/portfolio-prog2006/lab01)
- Lab02 - lists and recursion in Haskell - [Link Lab02](https://gitlab.com/portfolio-prog2006/lab02/-/tree/main/Lab02?ref_type=heads)
- Lab03 - more lists and recursion, more syntax - [Link Lab03](https://gitlab.com/portfolio-prog2006/lab03)
- Lab04 - using 'Maybe' and 'Either' - [Link Lab04](https://gitlab.com/portfolio-prog2006/lab04)

### Rust Projects
- Lab08 - numbers and types in Rust - [Link Lab08](https://gitlab.com/portfolio-prog2006/lab08)
- Lab09 - json structs and crates in Rust - [Link Lab09](https://gitlab.com/portfolio-prog2006/lab09)
- Lab10 - axum and tokio, web APIs in Rust - [Link Lab10](https://gitlab.com/portfolio-prog2006/lab10)
- Lab11 - modular code and file manipulation in Rust - [Link Lab11](https://gitlab.com/portfolio-prog2006/lab11)
- Random Generator - using rand crate and working with hashmaps in Rust - [Link RandomGen](https://gitlab.com/portfolio-prog2006/randomgen/-/tree/main/random_gen?ref_type=heads)
- Snake Game - an implementation of the retro Snake game in Rust - [Link SnakeGame](https://gitlab.com/portfolio-prog2006/snakegame/-/tree/main/portfolio_snake?ref_type=heads)

## Demo Video
- [Portfolio Demo on YouTube](https://www.youtube.com/watch?v=x8UGOe5SIKg)

(On YouTube, see timestamps in the description box to view specific projects)